BEGIN TRANSACTION;

CREATE TYPE "source_enum" AS ENUM ('GITLAB', 'GITHUB', 'BITBUCKET');
CREATE TABLE "user" (
	"id"	INTEGER NOT NULL,
	"source"	"source_enum" NOT NULL,
	"username"	TEXT NOT NULL,
	"name"	TEXT NOT NULL,
	"avatar_url"	TEXT NOT NULL,
	"web_url"	TEXT NOT NULL,
	PRIMARY KEY("id", "source")
);
CREATE TABLE "project" (
	"id"	INTEGER NOT NULL,
	"source"	"source_enum" NOT NULL,
	"user_id"	INTEGER NOT NULL,
	"name"	TEXT NOT NULL,
	"date_active"	TEXT NOT NULL,
	PRIMARY KEY("id", "source"),
	FOREIGN KEY("user_id", "source") REFERENCES "user"("id", "source")
);
CREATE TABLE "commit" (
	"id"	TEXT NOT NULL,
	"source"	"source_enum" NOT NULL,
	"project_id"	INTEGER NOT NULL,
	"user_id"	INTEGER NOT NULL,
	"title"	TEXT NOT NULL,
	"committed_date"	TEXT NOT NULL,
	PRIMARY KEY("id", "source"),
	FOREIGN KEY("user_id", "source") REFERENCES "user"("id", "source"),
	FOREIGN KEY("project_id", "source") REFERENCES "project"("id", "source")
);

COMMIT;
