BEGIN TRANSACTION;

DROP TABLE "user" CASCADE;
ALTER TABLE "vcs_user" RENAME TO "user";
ALTER TABLE "user" DROP COLUMN "user_id";
ALTER TABLE "project" RENAME CONSTRAINT "project_vcs_user_id_fkey" TO "project_user_id_fkey";
ALTER TABLE "commit" RENAME CONSTRAINT "commit_vcs_user_id_fkey" TO "commit_user_id_fkey";

COMMIT;