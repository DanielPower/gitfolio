BEGIN TRANSACTION;

ALTER TABLE "user" RENAME TO "vcs_user";
ALTER TABLE "project" RENAME COLUMN "user_id" TO "vcs_user_id";
ALTER TABLE "project" RENAME CONSTRAINT "project_user_id_fkey" TO "project_vcs_user_id_fkey";
ALTER TABLE "commit" RENAME COLUMN "user_id" TO "vcs_user_id";
ALTER TABLE "commit" RENAME CONSTRAINT "commit_user_id_fkey" TO "commit_vcs_user_id_fkey";

CREATE TABLE "user" (
    "id"    SERIAL UNIQUE NOT NULL PRIMARY KEY,
    "username"  TEXT NOT NULL UNIQUE,
    "name"  TEXT NOT NULL
);
ALTER TABLE "vcs_user" ADD COLUMN "user_id" INTEGER;
ALTER TABLE "vcs_user" ADD CONSTRAINT "vcs_user_user_id_fkey" 
    FOREIGN KEY ("user_id") REFERENCES "user"("id");
UPDATE "vcs_user" SET "user_id" = 0;
ALTER TABLE "vcs_user" ALTER COLUMN "user_id" SET NOT NULL;

COMMIT;