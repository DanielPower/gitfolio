import psycopg2
import os
import yaml

from app import app


@app.cli.command("resetdb")
def reset_database():
    choice = input(
        "Are you sure you want to reset the database? All data will be lost. [y/N]: "
    )
    if choice.upper() == "Y":
        print("Clearing database")
        db = get_db(name="postgres")
        db.autocommit = True
        cursor = db.cursor()
        database_name = app.config["DATABASE_NAME"]
        cursor.execute(f"""DROP DATABASE IF EXISTS "{database_name}";""")
        cursor.execute(f"""CREATE DATABASE "{database_name}";""")
        db.close()
        print("Performing initial migration")
        db = get_db()
        migrations = get_migrations()
        for migration in migrations:
            run_upgrade(db, migration)
        set_current_migration(db, migrations[-1]["name"])
        db.close()


@app.cli.command("upgradedb")
def upgrade_database():
    db = get_db()
    migrations = get_migrations()
    current_migration_index = get_current_migration_index(db, migrations)
    for migration in migrations[current_migration_index + 1 :]:
        run_upgrade(db, migration)
    set_current_migration(db, migrations[-1]["name"])
    db.close()


@app.cli.command("downgradedb")
def downgrade_database():
    db = get_db()
    migrations = get_migrations()
    current_migration_index = get_current_migration_index(db, migrations)
    if current_migration_index != 0:
        run_downgrade(db, migrations[current_migration_index])
        set_current_migration(db, migrations[current_migration_index - 1]["name"])
    db.close()


def get_db(
    host=app.config["DATABASE_HOST"],
    user=app.config["DATABASE_USER"],
    password=app.config["DATABASE_PASSWORD"],
    name=app.config["DATABASE_NAME"],
):
    return psycopg2.connect(
        f"host={host} user={user} password={password} dbname={name}"
    )


def get_migrations():
    with open("./migrations/migrations.yaml", "r") as file:
        return yaml.load(file, Loader=yaml.SafeLoader)


def get_current_migration_name(db):
    cursor = db.cursor()
    cursor.execute('SELECT "name" FROM "_migration" LIMIT 1')
    return cursor.fetchone()[0]


def get_current_migration_index(db, migrations):
    current_migration_name = get_current_migration_name(db)
    for index, migration in enumerate(migrations):
        if migration["name"] == current_migration_name:
            return index


def run_upgrade(db, migration):
    cursor = db.cursor()
    name = migration["name"]
    print(f"running upgrade: {name}")
    with open(f"./migrations/{name}/upgrade.sql", "r") as file:
        cursor.execute(file.read())


def run_downgrade(db, migration):
    cursor = db.cursor()
    name = migration["name"]
    print(f"running downgrade: {name}")
    with open(f"./migrations/{name}/downgrade.sql", "r") as file:
        cursor.execute(file.read())


def set_current_migration(db, name):
    cursor = db.cursor()
    cursor.execute(
        f"""
        DROP TABLE IF EXISTS "_migration";
        CREATE TABLE "_migration" (
            "name"  TEXT NOT NULL
        );
        INSERT INTO "_migration" VALUES('{name}')
        """
    )
