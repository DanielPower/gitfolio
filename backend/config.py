class Config:
    DATABASE_USER = "daniel"
    DATABASE_PASSWORD = "password"
    DATABASE_HOST = "localhost"
    DATABASE_NAME = "gitfolio"

    SQLALCHEMY_DATABASE_URI = (
        f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}"
        f"@{DATABASE_HOST}/{DATABASE_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
