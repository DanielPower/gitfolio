from sqlalchemy import and_
from app import app, db
from app.models.commit import Commit
from app.models.vcs_user import VcsUser
import sys
import json


@app.route("/commits/<int:user_id>")
def get_user_commits(user_id):
    commits = []
    vcs_users = VcsUser.query.filter_by(user_id=user_id).all()
    for vcs_user in vcs_users:
        commits += [
            commit.get_api_representation()
            for commit in Commit.query.filter_by(vcs_user_id=vcs_user.id).all()
        ]

    return json.dumps(commits)
