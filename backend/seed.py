from app import app, db
from app.models.user import User
from app.models.vcs_user import VcsUser
from app.models.enums import Source


@app.cli.command("seed")
def create_seed_data():
    user = User.add(username="dan", name="Daniel Power",)
    VcsUser.add(
        id=625102,
        source=Source.GITLAB,
        username="danielpower",
        name="Daniel Power",
        avatar_url="https://secure.gravatar.com/avatar/dd687a54055f366d6da421b49831af96?s=80&d=identicon",
        web_url="https://gitlab.com/DanielPower",
        user_id=user.id,
    )
    VcsUser.add(
        id=12166320,
        source=Source.GITHUB,
        username="danielpower",
        name="Daniel Power",
        avatar_url="https://avatars3.githubusercontent.com/u/12166320?v=4",
        web_url="https://api.github.com/users/DanielPower",
        user_id=user.id,
    )
