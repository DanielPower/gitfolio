import enum


class Source(enum.Enum):
    GITLAB = 1
    GITHUB = 2
    BITBUCKET = 3
