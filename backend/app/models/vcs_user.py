from sqlalchemy import ForeignKey

from app import db
from app.models.base_model import BaseModel
from app.models.enums import Source


class VcsUser(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.Enum(Source), primary_key=True)
    username = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    avatar_url = db.Column(db.String, nullable=False)
    web_url = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, ForeignKey("user.id"))
