from sqlalchemy.inspection import inspect

from app import db


class BaseModel:
    @classmethod
    def add(cls, *args, should_commit=True, **kwargs):
        row = cls(*args, **kwargs)
        db.session.add(row)
        if should_commit:
            db.session.commit()
        return row

    @classmethod
    def add_or_update(cls, *args, should_commit=True, **kwargs):
        row = cls.query.get([kwargs[key.name] for key in inspect(cls).primary_key])
        if row:
            for key, value in kwargs.items():
                setattr(row, key, value)
        else:
            row = cls(*args, **kwargs)
            db.session.add(row)
        if should_commit:
            db.session.commit()
        return row
