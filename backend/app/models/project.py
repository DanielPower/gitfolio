from sqlalchemy import ForeignKeyConstraint

from app import db
from app.models.base_model import BaseModel
from app.models.enums import Source
from app.models.vcs_user import VcsUser


class Project(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.Enum(Source), primary_key=True)
    vcs_user_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String, nullable=False)
    date_active = db.Column(db.String, nullable=False)
    __table_args__ = (
        ForeignKeyConstraint([vcs_user_id, source], [VcsUser.id, VcsUser.source]),
        {},
    )
