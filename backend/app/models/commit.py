from sqlalchemy import ForeignKeyConstraint

from app import db
from app.models.base_model import BaseModel
from app.models.vcs_user import VcsUser
from app.models.enums import Source


class Commit(db.Model, BaseModel):
    id = db.Column(db.String, primary_key=True)
    source = db.Column(db.Enum(Source), primary_key=True)
    vcs_user_id = db.Column(db.Integer, nullable=False)
    project_id = db.Column(db.Integer, nullable=False)
    title = db.Column(db.String, nullable=False)
    committed_date = db.Column(db.String, nullable=False)
    __table_args__ = (
        ForeignKeyConstraint([vcs_user_id, source], [VcsUser.id, VcsUser.source]),
        {},
    )

    def get_api_representation(self):
        return {
            "id": self.id,
            "title": self.title,
            "committed_date": self.committed_date,
        }
