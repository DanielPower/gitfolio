import os
import sqlite3

import yaml
from flask import Flask, g, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from config import Config

app = Flask(__name__)
app.config.from_object(Config())
cors = CORS(app, resources={r"/*": {"origins": "*"}})
db = SQLAlchemy(app)
