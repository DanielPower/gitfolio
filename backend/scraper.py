import json

import requests

from app import app
from app.models.enums import Source
from app.models.project import Project
from app.models.commit import Commit
from app.models.user import User
from app.models.vcs_user import VcsUser

API_PREFIX = "https://gitlab.com/api/v4"


@app.cli.command("scrape")
def scrape():
    for user in User.query.all():
        for gitlab_user in VcsUser.query.filter_by(
            user_id=user.id, source=Source.GITLAB
        ):
            scrape_gitlab(gitlab_user)


def scrape_gitlab(gitlab_user):
    project_dicts = request_all_from_paginated_route(
        f"{API_PREFIX}/users/{gitlab_user.id}/projects", simple=True
    )
    for project_dict in project_dicts:
        project = Project.add_or_update(
            id=project_dict["id"],
            source=Source.GITLAB,
            vcs_user_id=gitlab_user.id,
            name=project_dict["name"],
            date_active=project_dict["last_activity_at"],
        )
        commit_dicts = request_all_from_paginated_route(
            f"{API_PREFIX}/projects/{project.id}/repository/commits",
        )
        for commit_dict in commit_dicts:
            Commit.add_or_update(
                id=commit_dict["id"],
                source=Source.GITLAB,
                vcs_user_id=gitlab_user.id,
                project_id=project.id,
                title=commit_dict["message"],
                committed_date=commit_dict["committed_date"],
            )


def request_all_from_paginated_route(route, page=1, **kwargs):
    params = "".join([f"&{key}={value}" for key, value in kwargs.items()])
    response = requests.get(f"{route}?page={page}{params}")
    results = json.loads(response.text)
    if response.headers["X-Next-Page"]:
        results += request_all_from_paginated_route(route, page=page + 1, **kwargs)
    return results
