import React from 'react';
import PropTypes from 'prop-types';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import styles from './ActivityHeatmap.module.css';

const DAY = 86400000; // 1000 * 60 * 60 * 24
const NODE_SIZE = 15;
const NODE_SPACING = 2;
const ACTIVITY_COLORS = [
  '#7F557D',
  '#967394',
  '#AD92AC',
  '#C4B1C3',
  '#DCD0DB',
  '#EEEEEE',
];

// Generates a heatmap for commits, comments, etc. Like what GitHub/GitLab show
const ActivityHeatmap = ({ startDate, length, dates, reverse }) => {
  if (reverse) startDate.setDate(startDate.getDate() - length);
  const firstDayOfWeek = startDate.getDay();
  const numWeeks = Math.ceil(length / 7);
  const weeks = new Array(numWeeks);
  let maxCommits = 1;
  for (let i = 0; i < numWeeks; i += 1) {
    weeks[i] = { id: i, values: [] };
    for (let j = 0; j < 7; j += 1) {
      const id = Math.floor(i * 7 + j - firstDayOfWeek);
      const date = new Date(startDate);
      date.setDate(date.getDate() + id);
      if (id > 0 && id <= length) {
        weeks[i].values[j] = { id, date, value: 0, shouldRender: true };
      } else {
        weeks[i].values[j] = { id, shouldRender: false };
      }
    }
  }

  dates.forEach((date) => {
    const index = Math.floor((date - startDate) / DAY);
    const week = Math.ceil(index / 7);
    const dayOfWeek = date.getDay();
    if (week >= 0 && week < weeks.length) {
      const day = weeks[week].values[dayOfWeek];
      day.value += 1;
      if (day.value > maxCommits) {
        maxCommits = day.value;
      }
    }
  });

  const heatmapWidth = weeks.length * (NODE_SIZE + NODE_SPACING);
  const heatmapHeight = 7 * (NODE_SIZE + NODE_SPACING);

  return (
    <svg className={styles.rect} width={heatmapWidth} height={heatmapHeight}>
      {weeks.map((week, x) =>
        week.values.map((day, y) => {
          // Make more beautiful, use less hardcoded values
          const color = ((value) => {
            if (value >= 20) {
              return ACTIVITY_COLORS[0];
            }
            if (value >= 10) {
              return ACTIVITY_COLORS[1];
            }
            if (value >= 5) {
              return ACTIVITY_COLORS[2];
            }
            if (value >= 3) {
              return ACTIVITY_COLORS[3];
            }
            if (value >= 1) {
              return ACTIVITY_COLORS[4];
            }
            return ACTIVITY_COLORS[5];
          })(day.value);

          const translateX = x * (NODE_SIZE + NODE_SPACING);
          const translateY = y * (NODE_SIZE + NODE_SPACING);

          return (
            day.shouldRender && (
              <OverlayTrigger
                key={day.id}
                overlay={
                  <Tooltip>
                    {day.date.toLocaleDateString('en-CA')}
                    <br />
                    {day.value} contributions
                  </Tooltip>
                }
              >
                <rect
                  width={NODE_SIZE}
                  height={NODE_SIZE}
                  className={styles.cell}
                  transform={`translate(${translateX}, ${translateY})`}
                  fill={color}
                />
              </OverlayTrigger>
            )
          );
        }),
      )}
    </svg>
  );
};

ActivityHeatmap.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  length: PropTypes.number.isRequired,
  dates: PropTypes.arrayOf(PropTypes.instanceOf(Date)).isRequired,
  reverse: PropTypes.bool,
};

ActivityHeatmap.defaultProps = {
  reverse: false,
};

export default ActivityHeatmap;
