export default {
  primary: { hex: '#83B5D1' },
  seconday: { hex: '#7698B3' },
  accent1: { hex: '#726E97' },
  accent2: { hex: '#7F557D' },
  accent3: { hex: '#673C4F' },
};
