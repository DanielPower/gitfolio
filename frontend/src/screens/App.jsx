import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import Profile from './Profile';

const App = () => {
  return (
    <Router>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand>
          <Link to="/">Gitfolio</Link>
        </Navbar.Brand>
        <Navbar.Collapse>
          <Navbar.Text>
            <Link to="/profile">Profile</Link>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>

      <Switch>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
