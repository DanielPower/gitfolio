import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGithub,
  faGitlab,
  faBitbucket,
} from '@fortawesome/free-brands-svg-icons';
import axios from 'axios';
import CommitHeatmap from '../components/ActivityHeatmap/ActivityHeatmap';

const Profile = () => {
  const [commits, setCommits] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:5000/commits/1').then((res) => {
      const dates = [];
      res.data.forEach((commit) => {
        dates.push(new Date(commit.committed_date));
      });
      setCommits(dates);
    });
  }, []);

  return (
    <>
      <Container>
        <Row className="justify-content-md-center">
          <Card style={{ width: '40rem' }}>
            <Card.Body>
              <Card.Title>Daniel Power</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Full Stack Developer
              </Card.Subtitle>
              <Card.Text>I write code and shit. It&apos;s what I do.</Card.Text>
              <Card.Text>
                <FontAwesomeIcon icon={faGithub} size="lg" />
                <FontAwesomeIcon icon={faGitlab} size="lg" />
                <FontAwesomeIcon icon={faBitbucket} size="lg" />
              </Card.Text>
            </Card.Body>
          </Card>
        </Row>
        <Row className="justify-content-md-center">
          <CommitHeatmap
            startDate={new Date()}
            length={365}
            dates={commits}
            reverse
          />
        </Row>
      </Container>
    </>
  );
};

export default Profile;
